# !/bin/sh
# Title: recall
# Author:
# Date: 00/00/0000
# Purpose: recall tips and notes from store database file
# Update:

# variables
store_data="$HOME/scripts/store.db"
if [ "$#" -eq 0 ] ; then
 more $store_data
else
 # note ${PAGER:-more} allows default pager to be used unless nothing set then more used.
 grep -i "$@" $store_data | ${PAGER:-more}
fi

if [ "$1" = "-h"] ; then
    echo "Usage: $0 details"
    exit 0
fi
sclog=security_check.log

# Define a variable for date
now=`date`

# Create a header at the top of the security log

echo "*******************************" >> $sclog

# add hostname to the security log
echo "*** Security report for: $HOSTNAME " >> $sclog

# add date to the security log
echo "*** Report Date: $now " >> $sclog

grep wheel /etc/group >> $sclog
